


// 1. [Insert One Method]

db.hotel.insert({
	name: "single", 
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities", 
	rooms_available: 10,
	isAvailable: false
})




// 2. [Insert Many Method ]


db.hotel.insertMany([
	{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation", 
	rooms_available: 5,
	isAvailable: false
	},
	{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway", 
	rooms_available: 15,
	isAvailable: false
	}
])




// 3. [Find One Method]

 db.hotel.find({name: "double"})


 // 4. [Update One Method]

 db.hotel.updateOne(
	{
		name: "queen"
	}, 
	{
		$set: {
			rooms_available: 0
		}
	}

)



 // 4 . [ Delete Many Method]

 db.hotel.deleteMany({rooms_available: 0})